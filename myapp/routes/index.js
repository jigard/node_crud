
var express = require('express');
var router = express.Router();
var db = require('../model/db.js');
var Data = require('../controllers/dataController.js');
var Mail = require('../controllers/mailController.js');

var path = require('path');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/images');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
var upload = multer({ storage: storage });



/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });


//CRUD 
router.get('/', Data.listData);
router.get('/add_data', Data.listSaveData);
router.post('/insert', upload.single('image'), Data.saveData);
router.get('/viewupdate/:id', Data.updateViewData);
router.put('/update/:id', upload.single('image'), Data.updateData);
router.get('/delete/:id', Data.deleteData);


//Mail
router.get('/mail', Mail.sendViewMail);
router.post('/sendmail', Mail.sendMail);



module.exports = router;
console.log('route');
