var route = require('../routes/index.js');
var transporter = require('../model/mail_con.js');

//View Mail Page
exports.sendViewMail = function (req, res) {
    res.render('mail.hbs');
}

//Send Mail
exports.sendMail = function (req, res) {
    var mailoption = {
        from: "jigard.mobio@gmail.com",
        to: req.body.to,
        subject: req.body.subject,
        text: req.body.message

    }
    // console.log('from '+ from);
    // console.log('to ' + req.body.to);
    // console.log('sub ' + req.body.subject);
    // console.log('message ' + req.body.message);


    transporter.sendMail(mailoption, function (err, result) {
        if (err) throw err;
        console.log('mail sent');
        res.sendFile('sendmail.hbs', { result: req.body });
    });
    res.render('mail.hbs');
}
console.log('mailcontroller');