var route = require('../routes/index.js');
var con = require('../model/db.js');

//View List Data
exports.listData = function (req, res) {
  var sql = "SELECT * FROM demo";
  var listdb = con.query(sql, function (err, results) {
    if (err) throw err;
    console.log(sql);
    res.render('index.hbs', {
      results: results
    });
  })
}

//View Add Detail Form
exports.listSaveData = function (req, res) {
  res.render('add_data.hbs');
}

//Add Data
exports.saveData = function (req, res) {
  // var validator = new val(req.body,{
  //   name:'required|max:255|string',
  //   email:'required|string|email|max:255',
  //   mobile_number:'required|numeric|digits:10'
  // });
  // validator.check().then(function(matched){
  //     if(!matched) 
  //     res.status(422).send(validator.errors);
  //     // res.sendFile('add_data.hbs');
  // })
  var data = { name: req.body.name, email: req.body.email, mobile_number: req.body.mobile_number, image: req.file.filename };
  var adddb = con.query("INSERT INTO demo SET ?", data, function (err, result) {
    if (err) throw err;
    res.redirect('/');
  })
}

//View Update Data Form
exports.updateViewData = function (req, res) {
  var sql = "SELECT * FROM demo WHERE id = '" + [req.params.id] + "'";
  var updateView = con.query(sql, function (err, results) {
    if (err) throw err;
    res.render('update.hbs', {
      results: results
    });
  });
}

//Update detail
exports.updateData = function (req, res) {
  if (req.file) {
    var sql = "UPDATE demo SET name='" + req.body.name + "', email='" + req.body.email + "', mobile_number='" + req.body.mobile_number + "', image='" + req.file.filename + "' WHERE id=" + req.params.id;
    var updateData = con.query(sql, function (err, result) {
      if (err) throw err;
    });
  } else {
    var sql = "UPDATE demo SET name='" + req.body.name + "', email='" + req.body.email + "', mobile_number='" + req.body.mobile_number + "' WHERE id=" + req.params.id;
    var updateData = con.query(sql, function (err, result) {
      if (err) throw err;
    });
  }
  res.redirect('/');
}

//Delete Data
exports.deleteData = function (req, res) {
  var sql = "DELETE FROM demo WHERE id ='" + req.params.id + "'";
  var deleteData = con.query(sql, function (err, result) {
    console.log(sql);
    if (err) throw err;
    res.redirect('/');
  });
}