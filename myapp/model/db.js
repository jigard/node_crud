var mysql = require('mysql');

var con = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    database:'node'
});

con.connect(function(err) {
    if (err) throw err;
    console.log('db connected');
});
con.on('error', function(err) {
    console.log("[mysql error]",err);
});

module.exports = con;
console.log('db');